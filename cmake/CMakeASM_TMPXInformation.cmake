# CMakeASM_TMPXInformation.cmake
##############################################################################
# Support for TMPx 1.1.0 / STYLE Commodore 64 Assembler

# http://turbo.style64.org/docs/tmpx-command-line-options

#	-h/--help	Shows a summary of these command line options.
#	-?/--usage	Shows a very brief reminder of the simplest use case.
#	/--version	Shows version information and credits.
#	-a/--ascii	Assembles in ASCII mode, with no conversion to PETSCII.
#	-q/--quiet	Suppresses version notice and other extraneous information while assembling.
#	-i/--in <file>	Assembles the given file.
#	-I/	When specified, labels are treated case-insensitive.
#	-l/--list <file>	List output is directed to the given file.
#	-o/--out <file>	Binary machine code output from the assembler is written to the given file.
#	-P/--proff	Start assembly with an implied .proff
#	-s/--symbol	Generate symbol output.
#	-M/--hide-macros	Suppress macro expansions in list output.
#	-D/--define <define>	Pass the assembler an additional label definition in the pattern of '-D LABELNAME' (which assigns a defualt value of 1 to the label) or '-D LABELNAME=EXPR'.
#	/--set-undoc-off	Regard any undocumented (aka illegal) opcodes as errors.
#	/--set-dtv-off	Regard any DTV-specific opcodes as errors.
#
##############################################################################
#
#	A basic program call to assemble a source file named "code.ms" into an object file named "code.o":
#
#	    TMPx -i code.ms -o code.o
#

MESSAGE("-¤# Information for Commodore 64 platform! #¤-")


SET(ASM_DIALECT "_TMPX")

SET(CMAKE_ASM${ASM_DIALECT}_SOURCE_FILE_EXTENSIONS ms;s;asm)


#set default arguments here
IF(UNIX)
	SET(CMAKE_ASM_TMPX_COMPILER_ARG1 "-f elf")
ELSE(UNIX)
	SET(CMAKE_ASM_TMPX_COMPILER_ARG1 "-f win32")
ENDIF(UNIX)

# This section exists to override the one in CMakeASMInformation.cmake
# (the default Information file). This removes the <FLAGS>
# thing so that your C compiler flags that have been set via
# set_target_properties don't get passed to TMPx and confuse it.
IF(NOT CMAKE_ASM${ASM_DIALECT}_COMPILE_OBJECT)

	#TODO: Change nasm args to correct tmpx arguments
	SET(CMAKE_ASM${ASM_DIALECT}_COMPILE_OBJECT "<CMAKE_ASM${ASM_DIALECT}_COMPILER> -o <OBJECT> <SOURCE>")

ENDIF(NOT CMAKE_ASM${ASM_DIALECT}_COMPILE_OBJECT)

INCLUDE(CMakeASMInformation)
SET(ASM_DIALECT)
