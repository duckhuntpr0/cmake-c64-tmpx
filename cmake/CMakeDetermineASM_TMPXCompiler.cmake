# CMakeDetermineASM_TMPXCompiler.cmake
##############################################################################
# Support for TMPx 1.1.0 / STYLE Commodore 64 Assembler

SET(ASM_DIALECT "_TMPX")
SET_PROPERTY(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS FALSE)

MESSAGE("-¤# Looking for TMPx Assembler for Commodore 64 platform! #¤-")

SET(CMAKE_ASM_TMPX_COMPILER "${CMAKE_BINARY_DIR}/tools/TMPx_v1.1.0-STYLE/linux-x86_64/tmpx")
#SET(CMAKE_ASM_TMPX_COMPILER "tmpx")

#MESSAGE(STATUS "TMPX Determine Compiler fitta")

IF(UNIX)
	SET(CMAKE_ASM${ASM_DIALECT}_COMPILER_LIST ${_CMAKE_TOOLCHAIN_PREFIX}tmpx)
ENDIF()

# TODO: Properly determine exen for other host platforms
# (The Windows exe would be 'TMPx.exe' but possibly 'tmpx.exe' or 'TMPX.EXE')

INCLUDE(CMakeDetermineASMCompiler)

SET(ASM_DIALECT)
